import type { NextPage } from "next";
import Link from "next/link";
import Head from "next/head";
import Layout from "../../components/layout";
//import Script from "next/script";
const FirstPost: NextPage = () => {
  return (
    <Layout>
      <Head>
        <title>First Post</title>
      </Head>
      {/* <Script
        src="https://connect.facebook.net/en_US/sdk.js"
        strategy="lazyOnload"
        onLoad={() => {
          console.log("Script loaded");
        }}
      /> */}
      <h1>First Post</h1>
    </Layout>
  );
};

export default FirstPost;
